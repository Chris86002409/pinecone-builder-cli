const fs = require('fs')

module.exports.CreateDirectory = async (directoryPath) => {
    console.log('TRYING TO CREATE DIRECTORY', directoryPath)
    try {
        const newDirectory = fs.mkdirSync(directoryPath, { recursive: true })
        return newDirectory
    } catch (err) {
        console.error('IMPOSSIBLE TO CREATE DIRECTORY', directoryPath, err)
        return err
    }
}

module.exports.CreateFile = async ({ filePath, content }) => {
    console.log('TRYING TO CREATE FILE', filePath)
    try {
        const newFile = fs.writeFileSync(filePath, content)
        return newFile
    } catch (err) {
        console.error('IMPOSSIBLE TO CREATE FILE', filePath, err)
        return err
    }
}
