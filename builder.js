const { v1 } = require('uuid')
const { CreateDirectory, CreateFile } = require('./utils/fs-utils')

module.exports.BuildProcess = async (params) => {
    await InitProject(params)
}

const InitProject = async (params) => {
    const id = v1()
    let body = {
        id
    }
    body = { ...body, ...params }
    const directoryPath = `./tests/${id}`
    const configFilePath = `${directoryPath}/config.json`
    try {
        await CreateDirectory(directoryPath)
        const createdFile = await CreateFile({ filePath: configFilePath, content: JSON.stringify(body, null, 4) })
        return createdFile
    } catch (error) {
        return error
    }
}
